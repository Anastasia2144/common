package db;

import communication.Response;
import communication.ResponseType;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DBRequest {

    public static Response create(Connection connection, String word, String meaning) {

        Response response = new Response();

        try {
            PreparedStatement statement = connection.prepareStatement("INSERT INTO words(word, meaning) VALUES (?, ?)");
            statement.setString(1, word);
            statement.setString(2, meaning);
            statement.executeUpdate();

            response.setType(ResponseType.OK);
            response.getResult().put(word, "слово добавлено в словарь.");

        } catch (SQLException e) {
            response.setType(ResponseType.ERROR);
            if(e.getMessage().contains("Duplicate"))
                response.getResult().put(word, "такое слово уже существует!");
            else
                response.getResult().put(word, e.getMessage());
        }

        return response;
    }

    public static Response find(Connection connection, String word) {

        Response response = new Response();

        try {
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM words where word = ?");
            statement.setString(1, word);
            ResultSet resultSet = statement.executeQuery();

            if(resultSet.next()){
                response.setType(ResponseType.OK);
                response.getResult().put(word, resultSet.getString("meaning") );
            }else{
                response.setType(ResponseType.ERROR);
                response.getResult().put(word,  "такого слова нет!");
            }

        } catch (SQLException e) {
            response.getResult().put(word, e.getMessage());
        }

        return response;
    }

    public static Response update(Connection connection, String word, String newMeaning) {
        Response response = new Response();

        try {
            PreparedStatement statement = connection.prepareStatement("UPDATE words SET meaning = ? WHERE word = ?");
            statement.setString(1, newMeaning);
            statement.setString(2, word);

            int i = statement.executeUpdate();

            if (i == 0) {
                response.setType(ResponseType.ERROR);
                response.getResult().put(word, "такого слова нет!");

            }else{
                response.setType(ResponseType.OK);
                response.getResult().put(word, "значение слова успешно обновлено!");
            }

        }catch (SQLException e){
            response.getResult().put(word, e.getMessage());
        }

        return response;
    }

    public static Response delete(Connection connection, String word) {
        Response response = new Response();

        try {
            PreparedStatement statement =  connection.prepareStatement("DELETE FROM words WHERE word = ?");
            statement.setString(1, word);

            int i = statement.executeUpdate();

            if (i == 0){
                response.setType(ResponseType.ERROR);
                response.getResult().put(word, "такого слова нет!");
            }else {
                response.setType(ResponseType.OK);
                response.getResult().put(word, "удаление прошло успешно.");
            }

        } catch (SQLException e) {
            response.getResult().put(word, e.getMessage());
        }

        return response;
    }


    public static Response findMask(Connection connection, String mask) {

        Response response = new Response();

        try {
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM words where word LIKE ?");
            statement.setString(1, mask);
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                response.setType(ResponseType.OK);
                response.getResult().put(resultSet.getString("word"), resultSet.getString("meaning"));
            }

            if(response.getResult().isEmpty()){
                response.setType(ResponseType.ERROR);
                response.getResult().put(mask,  "по такой маске слов не найдено.");
            }
        } catch (SQLException e) {
            response.getResult().put(mask, e.getMessage());
        }

        return response;
    }
}
