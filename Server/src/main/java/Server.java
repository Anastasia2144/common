import com.google.gson.Gson;
import communication.*;
import db.DBManager;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Server {

    private final static String URL =
            "jdbc:mysql://localhost:3306/vocabulary?useUnicode=true&useSSL=true&useJDBCCompliantTimezoneShift=true" +
                    "&useLegacyDatetimeCode=false&serverTimezone=UTC";
    private final static String USERNAME = "root";
    private final static String PASSWORD = "root";

    private final static int port = 9101;

    public static void main(String[] args) throws ClassNotFoundException {
        System.out.println("Сервер запущен");

        try (Connection connection = DriverManager.getConnection(URL, USERNAME,PASSWORD)){

            System.out.println("База данных подключена");

            try(ServerSocket serverSocket = new ServerSocket(port);
                Socket client = serverSocket.accept();
                ConnectionClient connectionClient = new ConnectionClient(client);) {

                System.out.println("Клиент подключен");

                DBManager manager = new DBManager();
                Gson gson = new Gson();
                Request request;
                Response response;

                while (!client.isClosed()){

                    request = gson.fromJson(connectionClient.receive(), Request.class);

                    if (request == null ||request.getType() == null || request.getWord() == null){

                        response = new Response();
                        response.setType(ResponseType.ERROR);
                        response.getResult().put("Ошибка", "неверно сформированный запрос!");

                    }else {
                        if (request.getType().equals(RequestType.EXIT)) {
                            System.out.println("Клиент отключен");
                            break;
                        }

                        response = manager.doRequest(connection, request);
                    }

                    connectionClient.send(gson.toJson(response));
                }

                System.out.println("Сервер завершил работу");

            }catch (IOException e){
                System.out.println("Соединение прервано. Сервер завершает работу.");
            }
        } catch (SQLException e) {
            System.out.println("Ошибка при подключении базы данных");
        }
    }
}
