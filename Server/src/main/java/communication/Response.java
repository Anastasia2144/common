package communication;

import java.util.HashMap;
import java.util.Map;

public class Response {

    private ResponseType type;
    private Map<String, String> result = new HashMap<>();

    public Map<String, String> getResult() {
        return result;
    }

    public void setResult(Map<String, String> result) {
        this.result = result;
    }

    public ResponseType getType() {
        return type;
    }

    public void setType(ResponseType type) {
        this.type = type;
    }

}
